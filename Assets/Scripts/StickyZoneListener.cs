﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyZoneListener : MonoBehaviour
{
    public event Action<Collider> StickyZoneEntered;
    public event Action<Collider> StickyZoneLeft;

    // Update is called once per frame
    void OnTriggerEnter(Collider col)
    {
        StickyZoneEntered?.Invoke(col);
    }

    void OnTriggerExit(Collider col)
    {
        StickyZoneLeft?.Invoke(col);
    }
}
