﻿using UnityEngine;
using VRTK;

public class OnReleaseCheck : MonoBehaviour
{
    [SerializeField] private VRTK_InteractableObject m_selfObject;
    [SerializeField] private Stickable m_sticker;
    [SerializeField] private Rigidbody m_rigidbody;
    private bool m_grabbed;
    public void Start()
    {
        m_selfObject.InteractableObjectGrabbed += MSelfObjectOnInteractableObjectGrabbed;
        m_selfObject.InteractableObjectUngrabbed += MSelfObjectOnInteractableObjectUngrabbed;
    }

    public void OnDestroy()
    {
        m_selfObject.InteractableObjectGrabbed -= MSelfObjectOnInteractableObjectGrabbed;
        m_selfObject.InteractableObjectUngrabbed -= MSelfObjectOnInteractableObjectUngrabbed;

    }
    private void MSelfObjectOnInteractableObjectGrabbed(object sender, InteractableObjectEventArgs interactableObjectEventArgs)
    {
        m_grabbed = true;
    }

    private void MSelfObjectOnInteractableObjectUngrabbed(object sender, InteractableObjectEventArgs interactableObjectEventArgs)
    {
        m_grabbed = false;
    }

    private void Update()
    {
        if (!m_grabbed && m_sticker.ContactSticky.Count > 0)//not grabbed and stuck to wall
        {
            m_rigidbody.useGravity = false;
            m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        }
        else
        {
            m_rigidbody.useGravity = true;
            m_rigidbody.constraints = RigidbodyConstraints.None;
        }
    }
}