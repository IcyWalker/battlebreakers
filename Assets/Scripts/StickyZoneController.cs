﻿using System;
using System.Collections;
using UnityEngine;

public class StickyZoneController : MonoBehaviour
{
    [SerializeField] private StickyZoneListener[] m_stickyZoneListener;
    // Start is called before the first frame update
    void Start()
    {
        foreach (StickyZoneListener stickyZoneListener in m_stickyZoneListener)
        {
            stickyZoneListener.StickyZoneEntered += StickyZoneListenerOnStickyZoneEntered;
            stickyZoneListener.StickyZoneLeft += StickyZoneListenerOnStickyZoneLeft;
        }
    }

    void OnDestroy()
    {
        foreach (StickyZoneListener stickyZoneListener in m_stickyZoneListener)
        {
            stickyZoneListener.StickyZoneEntered -= StickyZoneListenerOnStickyZoneEntered;
            stickyZoneListener.StickyZoneLeft -= StickyZoneListenerOnStickyZoneLeft;
        }
    }

    private void StickyZoneListenerOnStickyZoneEntered(Collider collider)
    {
        Stickable stickable = collider.GetComponent<Stickable>();
        if (stickable != null)
        {
            stickable.StickZoneContacted(collider);
        }
    }
    private void StickyZoneListenerOnStickyZoneLeft(Collider collider)
    {
        Stickable stickable = collider.GetComponent<Stickable>();
        if (stickable != null)
        {
            stickable.StickZoneRemoved(collider);
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}