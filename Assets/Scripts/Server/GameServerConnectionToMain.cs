﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using DarkRift;
using DarkRift.Server;
using Hazel;
using Hazel.Tcp;
using UnityEngine;

public class GameServerConnectionToMain : MonoBehaviour
{
    private TcpConnection connection;
    // Start is called before the first frame update
    void Start()
    {
        string[] commandLineArgs = System.Environment.GetCommandLineArgs();
        //look for Master Server IP address
        string mastercloudip = "127.0.0.1";
        for (int i = 0; i < commandLineArgs.Length; i++)
        {
            if (commandLineArgs[i].ToLower() == "-mastercloudip")
            {
                mastercloudip = commandLineArgs[i + 1];
            }
        }

        //look for Master Server IP address
        int mastercloudport = 4200;
        for (int i = 0; i < commandLineArgs.Length; i++)
        {
            if (commandLineArgs[i].ToLower() == "-mastercloudport")
            {
                mastercloudport = int.Parse(commandLineArgs[i + 1]);
            }
        }

        string serverRequestedName = "TestName";
        for (int i = 0; i < commandLineArgs.Length; i++)
        {
            if (commandLineArgs[i].ToLower() == "-serverrequestedname")
            {
                serverRequestedName = commandLineArgs[i + 1];
            }
        }
        Debug.Log($"Connecting to master Server at {mastercloudip}:{mastercloudport} under the name {serverRequestedName}");
        NetworkEndPoint listener = new NetworkEndPoint(mastercloudip, mastercloudport);
        connection = new TcpConnection(listener);
        connection.DataReceived += ConnectionOnDataReceived;
        connection.Disconnected += ConnectionOnDisconnected;
        connection.Connect();

        ServerSpawnData spawnData = new ServerSpawnData(IPAddress.Any, 4201,IPVersion.IPv4);

        DarkRiftServer GameServerDRServer = new DarkRiftServer(spawnData);
        GameServerDRServer.ClientManager.ClientConnected += ClientManagerOnClientConnected;
        GameServerDRServer.ClientManager.ClientDisconnected += ClientManagerOnClientDisconnected;

        GameServerDRServer.Start();
    }

    private void ClientManagerOnClientDisconnected(object sender, ClientDisconnectedEventArgs clientDisconnectedEventArgs)
    {
        
    }

    private void ClientManagerOnClientConnected(object sender, ClientConnectedEventArgs clientConnectedEventArgs)
    {
        
    }

    private void ConnectionOnDisconnected(object sender, DisconnectedEventArgs disconnectedEventArgs)
    {
        //disconnected to local switch server
    }

    private static void ConnectionOnDataReceived(object sender, DataReceivedEventArgs dataReceivedEventArgs)
    {
        object[] data = SerializerDeserializerExtensions.Deserializer<object[]>(dataReceivedEventArgs.Bytes);

        switch (data[0])
        {
            case "SHOWSELF":
            {
                //Console.WriteLine("Show Self Requested");
                //object[] dataParse = { m_whatamI };
                //byte[] info = SerializerDeserializerExtensions.Serializer(dataParse);
                //connection.SendBytes(info, SendOption.Reliable);
                //ConnectToPlayerServer();
                break;
            }
            case "Server Ping":
            {
                Console.WriteLine("PING!");
                break;
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
