﻿using System;
using System.Threading.Tasks;
using BezierSolution;
using UnityEngine;

public class JumpingController : MonoBehaviour
{
    public bool isJumping;
    /// <summary>
    /// is used as a temp parent when in use
    /// </summary>
    [SerializeField] private GameObject m_jumpingSon;

    [SerializeField] private PlayerLocalPlayerInfo m_localPlayer;

    [SerializeField] private Transform m_localPlayerHolder;

    [SerializeField] private TeleportationCustomDash m_TeleportationCustomDash;
    [SerializeField] private Transform m_BezierSpine;

    [SerializeField] private GameObject m_playerJumpFloor;

    [SerializeField] private BezierWalkerWithSpeed m_JumpSpeedModifier;
    private bool pathComplete;
    public async void ProcessJumping(GameObject objectParent, Vector3 HighestPosition, RaycastHit EndPosition)
    {
        SafeParent safeParent = objectParent.GetComponent<SafeParent>();
        if (!safeParent)
        {
            isJumping = false;
        }
        else
        {
            //PlayerInteractionCol.transform.parent = objectParent.transform;
            m_jumpingSon.transform.position = new Vector3(m_localPlayer.MHeadSet.position.x,
                m_localPlayer.MPlayerzone.position.y, m_localPlayer.MHeadSet.position.z);
            Vector3[] pos = {m_jumpingSon.transform.position, HighestPosition, EndPosition.point};

            m_playerJumpFloor.transform.position = m_jumpingSon.transform.position;
            m_playerJumpFloor.transform.parent = m_TeleportationCustomDash.transform;
            m_TeleportationCustomDash.transform.parent = safeParent.GetSafeParent().transform;
            m_TeleportationCustomDash.UpdatePositions(pos);
            m_TeleportationCustomDash.transform.parent = safeParent.GetSafeParent().transform;
            m_BezierSpine.transform.parent = safeParent.GetSafeParent().transform;
            await Task.Delay(100);

            m_JumpSpeedModifier.Restart();
            m_JumpSpeedModifier.onPathCompleted.AddListener(PathFinished);
            m_JumpSpeedModifier.enabled = true;
            await Task.Delay(10);
            m_localPlayerHolder.parent = m_playerJumpFloor.transform;

            pathComplete = false;
            while (!pathComplete)
            {
                await Task.Delay(1000);
            }

            m_JumpSpeedModifier.onPathCompleted.RemoveListener(PathFinished);
            //m_playerJumpFloor.transform.parent = objectParent.transform;

            //m_TeleportationCustomDash.transform.parent = null;
            //m_BezierSpine.transform.parent = null;
            m_localPlayerHolder.parent = safeParent.GetSafeParent().transform;
            m_TeleportationCustomDash.transform.parent = null;
            m_BezierSpine.transform.parent = null;
            m_playerJumpFloor.transform.parent = null;
            isJumping = false;
        }
    }

    private void PathFinished()
    {
        pathComplete = true;
    }
}