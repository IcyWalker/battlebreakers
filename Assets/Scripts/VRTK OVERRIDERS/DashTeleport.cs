﻿using System;
using System.Collections;
using UnityEngine;
using VRTK;

public class DashTeleport : VRTK_DashTeleport
{
    [SerializeField] private GameObject m_MainPlayerHolderLocal;
    protected override void EndTeleport(object sender, DestinationMarkerEventArgs e)
    {
        base.EndTeleport(sender, e);
        m_MainPlayerHolderLocal.transform.parent = e.target;
    }

   
}