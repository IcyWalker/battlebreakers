﻿using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class BenjaminSlowmoJump : MonoBehaviour
{
    public VRTK_BezierPointerRenderer MBezierPointerRenderer;

    public VRTK_Pointer m_Pointer;

  
    private List<GameObject> isJumpingPoints;
    [SerializeField] private GameObject PlayerInteractionCol;
    [SerializeField] private JumpingController m_jumping;
    public void Start()
    {
        m_Pointer.ActivationButtonReleased += MPointerOnActivationButtonReleased;
        isJumpingPoints = new List<GameObject>();
    }

    private void MPointerOnActivationButtonReleased(object sender, ControllerInteractionEventArgs controllerInteractionEventArgs)
    {
        if (m_jumping.isJumping)//needs to be made global
        {
            return;
        }
        
        // check if valid to teleport
        if (m_Pointer.IsStateValid() && m_Pointer.enableTeleport)
        {
            GameObject[] allPointers = MBezierPointerRenderer.GetPointerObjects();
            if (allPointers.Length > 0)
            {
                VRTK_CurveGenerator generator = allPointers[0].GetComponent<VRTK_CurveGenerator>();
                GameObject[] grabGeneratedPoints = generator.GrabGeneratedItems();
                Vector3 highestPosition = grabGeneratedPoints[0].transform.position;
                foreach (GameObject grabGeneratedPoint in grabGeneratedPoints)
                {
                    if (highestPosition.y < grabGeneratedPoint.transform.position.y)
                    {
                        highestPosition = grabGeneratedPoint.transform.position;
                    }
                }
                //highestPosition
                Debug.Log("test");
                m_jumping.isJumping = true;
                //isJumpingPoints = new List<GameObject>();
                GameObject objectMovingTo = MBezierPointerRenderer.GetDestinationHit().collider.gameObject;

                //foreach (GameObject pointer in allPointers)
                //{
                //    GameObject newPointer = new GameObject("TempPoint");
                //    newPointer.transform.position = pointer.transform.position;
                //    newPointer.transform.rotation = pointer.transform.rotation;
                //    newPointer.transform.parent = objectMovingTo.transform;//target
                //    isJumpingPoints.Add(newPointer);
                //}

                m_jumping.ProcessJumping(objectMovingTo, highestPosition, MBezierPointerRenderer.GetDestinationHit());
            }
        }
    }

    
}