﻿using UnityEngine;

public class PlayerLocalPlayerInfo : MonoBehaviour
{
    [SerializeField] private Transform m_headSet;
    [SerializeField] private Transform m_left;
    [SerializeField] private Transform m_right;
    [SerializeField] private Transform m_playerzone;

    public Transform MHeadSet
    {
        get { return m_headSet; }
    }

    public Transform MLeft
    {
        get { return m_left; }
    }

    public Transform MRight
    {
        get { return m_right; }
    }

    public Transform MPlayerzone
    {
        get { return m_playerzone; }
    }
}