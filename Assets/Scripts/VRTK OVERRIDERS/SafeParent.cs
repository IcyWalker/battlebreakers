﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeParent : MonoBehaviour
{
    [SerializeField] private Transform m_safeParent;
    
    public Transform GetSafeParent()
    {
        return m_safeParent;
    }

}