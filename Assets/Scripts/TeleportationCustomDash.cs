﻿using System.Collections;
using System.Collections.Generic;
using BezierSolution;
using UnityEngine;

public class TeleportationCustomDash : MonoBehaviour
{
    [SerializeField] private Transform m_startPosition;
    [SerializeField] private Transform m_middlePosition;
    [SerializeField] private Transform m_endPosition;

    [SerializeField] private BezierSpline m_spline;

    private bool AutoContructSet;
    // Start is called before the first frame update
    void Start()
    {
       // m_spline.Reset();
        m_spline.Initialize(3);
    }

    public void UpdatePositions(Vector3[] pos)
    {
        if (pos.Length != 3)
        {
            return;

        }

        m_startPosition.transform.position = pos[0];
        m_middlePosition.transform.position = pos[1];
        m_endPosition.transform.position = pos[2];
    }
    // Update is called once per frame
    void Update()
    {
        if (m_spline[0].transform.position != m_startPosition.position ||
            m_spline[1].transform.position != m_middlePosition.position ||
            m_spline[2].transform.position != m_endPosition.position)
        {
            m_spline[0].transform.position = m_startPosition.position;
            m_spline[1].transform.position = m_middlePosition.position;
            m_spline[2].transform.position = m_endPosition.position;
            m_spline.AutoConstructSpline();

            //if (!AutoContructSet)
            //{
            //    AutoContructSet = true;

            //}
        }
    }
}
