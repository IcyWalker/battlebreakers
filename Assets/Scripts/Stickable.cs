﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Stickable : MonoBehaviour
{
    private List<Collider> m_contactSticky;
    [SerializeField] private Rigidbody m_rigidBody;

    public List<Collider> ContactSticky
    {
        get { return m_contactSticky; }
    }

    public void Start()
    {
        m_contactSticky = new List<Collider>();

    }

    protected virtual bool isStickable()
    {
        return true;
    }

    public void StickZoneContacted(Collider zone)
    {
        m_contactSticky.Add(zone);

    }
    public void StickZoneRemoved(Collider zone)
    {
        m_contactSticky.Remove(zone);
    }
}