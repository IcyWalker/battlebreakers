﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using DarkRift;
using DarkRift.Client;
using DarkRift.Client.Unity;
using TMPro;
using UnityEngine;

public class GameConnectionController : MonoBehaviour
{
    [SerializeField] private Transform[] GameStates;
    [SerializeField] private UnityClient m_client;
    [SerializeField] private TextMeshProUGUI m_connectingStatus;
    [SerializeField] private GameObject m_ConnectionAttemptButtons;
    [SerializeField] private GameObject m_ConnectionStatus;
    [SerializeField] private GameObject m_ConnectionFailedScreen;
    private enum CurrentConnectStates
    {
        NotConnected,
        State1
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public async void ConnectToMaster()
    {
        m_connectingStatus.text = "Connecting To Server";
        m_ConnectionAttemptButtons.SetActive(false);
        m_ConnectionStatus.SetActive(true);
        await Task.Delay(1000);
        m_client.ConnectInBackground(IPAddress.Parse("127.0.0.1"),4297,IPVersion.IPv4,Callback);

    }

    private async void Callback(Exception exception)
    {
        if (exception != null)
        {
            //failed to connect
            m_ConnectionStatus.SetActive(false);
            m_ConnectionFailedScreen.SetActive(true);
            FailedToConnectReset();
        }
        else
        {
            //successfull connect
            m_connectingStatus.text = "Gathering Data From Steam";
            SteamGrab();
        }
    }

    private async void SteamGrab()
    {
        string GrabSteamID = "guid_5461894844213"; //Grab from steam
        if (GrabSteamID == null)
        {
            //failed to connect
            m_client.Disconnect();
                
            m_ConnectionStatus.SetActive(false);
            m_ConnectionFailedScreen.SetActive(true);
            FailedToConnectReset();
        }
        else
        {
            object[] list = {GrabSteamID};
            object[] container = {"GrabProfile", list};
            bool RequiresResponce = true;
            object[] MessageTraveller = {"PlayerAuthentication", container, RequiresResponce };
            byte[] bytes = SerializerDeserializerExtensions.Serializer(MessageTraveller);
            using (DarkRiftWriter darkRiftWriter = DarkRiftWriter.Create())
            {
                darkRiftWriter.Write(bytes);
                using (Message m_LocalMessage = Message.Create(0, darkRiftWriter))
                {
                    m_client.SendMessage(m_LocalMessage, SendMode.Reliable);
                }
            }
        }
    }

    private async void FailedToConnectReset()
    {
        await Task.Delay(3000);
        m_ConnectionFailedScreen.SetActive(false);
        m_ConnectionAttemptButtons.SetActive(true);
    }

    private void MClientOnMessageReceived(object sender, MessageReceivedEventArgs messageReceivedEventArgs)
    {
        
    }

    private void MClientOnDisconnected(object sender, DisconnectedEventArgs disconnectedEventArgs)
    {
        
    }
}